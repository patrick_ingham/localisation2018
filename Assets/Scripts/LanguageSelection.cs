﻿using UnityEngine;
using UnityEngine.UI;
using System.Xml;
using System.IO;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

public class LanguageSelection : MonoBehaviour
{
	public List<Language> languages = new List<Language>();
	public GameObject flagButton;
	
	void Start()
	{
		TextAsset textData = Resources.Load<TextAsset>("Languages");
		parseLanguagesXML(textData.text);

		addSelectionButtons();
	}

	private void parseLanguagesXML(string xmlData)
	{
		XmlDocument xmlDoc = new XmlDocument();
		xmlDoc.Load(new StringReader(xmlData));

		string xmlPathPattern = "//languages/language";
		XmlNodeList myNodeList = xmlDoc.SelectNodes(xmlPathPattern);

		foreach (XmlNode node in myNodeList)
		{
			Language language = new Language(node.Attributes["identifier"].Value, node.InnerText);
			languages.Add(language);
		}

	}

	private void addSelectionButtons()
	{
		int xSteps = 0;
		foreach(Language language in languages)
		{
			GameObject fb = Instantiate(flagButton);
			fb.transform.SetParent(FindObjectOfType<Canvas>().transform);
			fb.GetComponent<Image>().sprite = language.FlagSprite;
			fb.GetComponent<RectTransform>().localPosition = new Vector3((xSteps*225) - Screen.width/2 + 175, 0, 0);
			fb.GetComponent<Button>().onClick.AddListener(delegate {
				Globals.languageIdentifier = language.Identifier;
				SceneManager.LoadScene("MainMenu");
			});

			xSteps++;
		}
	}

}