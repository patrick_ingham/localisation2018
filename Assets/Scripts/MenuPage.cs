﻿using UnityEngine;
using UnityEngine.UI;
using System.Xml;
using System.Text;
using System.Reflection;
using System.IO;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

public class MenuPage : MonoBehaviour {

	public Dictionary<string, string> menuItems;
	public GUIStyle btnStyle;
	
	void Start ()
	{
		menuItems = new Dictionary<string, string>();

		string fileToLoad = "menu-" + Globals.languageIdentifier;
				
		TextAsset menuList = Resources.Load<TextAsset>(fileToLoad);
		parseMenuFileXML(menuList.text);
		updateMenuItems();
	}

	private void parseMenuFileXML(string xmlData)
	{
		XmlDocument xmlDoc = new XmlDocument();
		xmlDoc.Load(new StringReader(xmlData));

		XmlNode myNode = xmlDoc.SelectSingleNode("MainMenu");

		foreach (XmlNode node in myNode)
		{
			menuItems.Add(node.Name, node.InnerText);
		}
	}

	void updateMenuItems(){
		GameObject.Find("M_START").GetComponent<Text>().text = menuItems["M_START"];
		GameObject.Find("M_INSTRUCTIONS").GetComponent<Text>().text = menuItems["M_INSTRUCTIONS"];
		GameObject.Find("M_OPTIONS").GetComponent<Text>().text = menuItems["M_OPTIONS"];
		GameObject.Find("M_BACK").GetComponent<Text>().text = menuItems["M_BACK"];

		addButtonActions();
	}

	void addButtonActions()
	{
		GameObject.Find("M_BACK").GetComponentInParent<Button>().onClick.AddListener(delegate {
			SceneManager.LoadScene("LanguageSelection");
		});
	}
}
		