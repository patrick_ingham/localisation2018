﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Language {

	public string Identifier {get;}
	public string FullName {get;}
	public Sprite FlagSprite {get; }

	public Language(string identifier, string fullName) {
		this.Identifier = identifier;
		this.FullName = fullName;
		this.FlagSprite = Resources.Load<Sprite>("flags/" + this.Identifier);
	}
	
}
